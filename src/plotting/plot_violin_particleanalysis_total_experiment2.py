# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 12:48:49 2020

@author: Ulla
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import pandas as pd
import seaborn as sns
plt.style.use('seaborn-talk')


# %%
#Load Data; insert the correct pathIn
pathIn=r"C:\Users\Ulla\Desktop\Fachvertiefung\Daten\200724_CLSM\daten_auswertung_intensity_corrected_py.csv"
df=pd.read_csv(pathIn)
#columns: AP= Area Patch, MP= MeanGreyValue Patch, AW= Area Wafer, MW= MeanGreyValue Wafer
#the number is the number of the evaluated patch
#WT or Cav determines between a wildtype and transfected/induced patch
#MeanGreyValues and Areas are determined with Particle Analysis in ImageJ
#all areas >3um^2 are removed from the original data

# %%
#overwrite the MeanGreyValue column by the value of MeanGrayValue per Area
df["MP1WT"]=df["MP1WT"]/df["AP1WT"]
df["MP2WT"]=df["MP2WT"]/df["AP2WT"]
df["MP3WT"]=df["MP3WT"]/df["AP3WT"]
df["MP4WT"]=df["MP4WT"]/df["AP4WT"]

df["MW1WT"]=df["MW1WT"]/df["AW1WT"]
df["MW2WT"]=df["MW2WT"]/df["AW2WT"]
df["MW3WT"]=df["MW3WT"]/df["AW3WT"]
df["MW4WT"]=df["MW4WT"]/df["AW4WT"]

df["MP1Cav"]=df["MP1Cav"]/df["AP1Cav"]
df["MP2Cav"]=df["MP2Cav"]/df["AP2Cav"]
df["MP3Cav"]=df["MP3Cav"]/df["AP3Cav"]
df["MP4Cav"]=df["MP4Cav"]/df["AP4Cav"]

df["MW1Cav"]=df["MW1Cav"]/df["AW1Cav"]
df["MW2Cav"]=df["MW2Cav"]/df["AW2Cav"]
df["MW3Cav"]=df["MW3Cav"]/df["AW3Cav"]
df["MW4Cav"]=df["MW4Cav"]/df["AW4Cav"]

# %%
#append all values for each condition in one column
data_WT=df["MP1WT"].append(df["MP2WT"], ignore_index=True)
data_WT=data_WT.append(df["MP3WT"], ignore_index=True)
data_WT=data_WT.append(df["MP4WT"], ignore_index=True)

data_Cav=df["MP1Cav"].append(df["MP2Cav"], ignore_index=True)
data_Cav=data_Cav.append(df["MP3Cav"], ignore_index=True)
data_Cav=data_Cav.append(df["MP4Cav"], ignore_index=True)

data_wafer=df["MW1WT"].append(df["MW2WT"], ignore_index=True)
data_wafer=data_wafer.append(df["MW3WT"], ignore_index=True)
data_wafer=data_wafer.append(df["MW4WT"], ignore_index=True)
data_wafer=data_wafer.append(df["MW1Cav"], ignore_index=True)
data_wafer=data_wafer.append(df["MW2Cav"], ignore_index=True)
data_wafer=data_wafer.append(df["MW3Cav"], ignore_index=True)
data_wafer=data_wafer.append(df["MW4Cav"], ignore_index=True)

# %%
#create new dataframe from the appended values and rename the columns 
data_wafer=data_wafer.to_frame()
data_wafer.columns = ['wafer']
data_WT=data_WT.to_frame()
data_WT.columns = ['WT']
data_Cav=data_Cav.to_frame()
data_Cav.columns = ['Cav']
frames = [data_wafer,data_WT,data_Cav]
result = pd.concat(frames,axis=1, sort=False)

# %%
#create violinplot from the new dataframe 'result' 
sns.set(font_scale=2)
sns.set_style("whitegrid")
sns.set_palette("tab10")
ax = sns.violinplot(data=result)
ax.set_ylabel('Intens/area')

#save plot image; insert savename
plt.savefig("violinplot_AB_labeling_2_corrected.pdf", bbox_inches="tight")