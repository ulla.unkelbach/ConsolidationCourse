# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 17:03:10 2020

@author: Ulla
"""
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import pandas as pd
plt.style.use('seaborn-talk')

# %%
#Load Data; insert the correct pathIn
pathIn=r"C:\Users\Ulla\Desktop\Fachvertiefung\Daten\200716_CLSM\data_py.csv"
df=pd.read_csv(pathIn)

#columns: AP= Area Patch, MP= MeanGreyValue Patch, AW= Area Wafer, MW= MeanGreyValue Wafer
#the number is the number of the evaluated patch
#MeanGreyValues and Areas are determined with Particle Analysis in ImageJ

# %%
#set variables for Plot legend
patch = mpatches.Patch(color='C1', label='Patch')
wafer = mpatches.Patch(color='C0', label='Wafer')

#create scatterplot for the patches
plt.scatter(df["AP1"],df["MP1"],color='C1',alpha=0.5, marker= '.')
plt.scatter(df["AP2"],df["MP2"],color='C1',alpha=0.5, marker= '.')
plt.scatter(df["AP3"],df["MP3"],color='C1',alpha=0.5, marker= '.')
plt.scatter(df["AP4"],df["MP4"],color='C1',alpha=0.5, marker= '.')
#create scatterplot for the wafer
plt.scatter(df["AW1"],df["MW1"],color='C0',alpha=0.5, marker= '.')
plt.scatter(df["AW2"],df["MW2"],color='C0',alpha=0.5, marker= '.')
plt.scatter(df["AW3"],df["MW3"],color='C0',alpha=0.5, marker= '.')
plt.scatter(df["AW4"],df["MW4"],color='C0',alpha=0.5, marker= '.')

plt.legend(handles=[patch,wafer],fontsize=20)
plt.grid(alpha=0.3)
plt.xlim(-0.5, 10)
#plt.ylim(0,1.2)
plt.xlabel('Area', fontsize=20)
plt.ylabel('Mean Grey Value', fontsize=20)

#save plot image; insert savename
plt.savefig("scatter_exp1.pdf", bbox_inches="tight")
plt.show()