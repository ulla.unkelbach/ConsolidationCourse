# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 15:49:13 2020

@author: Ulla
"""

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
plt.style.use('seaborn-talk')
sns.set_palette("tab10")

# %%
#Load Data; insert the correct pathIn
pathIn=r"C:\Users\Ulla\Desktop\Fachvertiefung\Daten\200724_CLSM\daten_auswertung_intensity_corrected_py.csv"
df=pd.read_csv(pathIn)
#columns: AP= Area Patch, MP= MeanGreyValue Patch, AW= Area Wafer, MW= MeanGreyValue Wafer
#the number is the number of the evaluated patch
#WT or Cav determines between a wildtype and transfected/induced patch
#MeanGreyValues and Areas are determined with Particle Analysis in ImageJ
#all areas >3um^2 are removed from the original data

# %%
#append all Area values for each condition in one column
dataA_WT=df["AP1WT"].append(df["AP2WT"], ignore_index=True)
dataA_WT=dataA_WT.append(df["AP3WT"], ignore_index=True)
dataA_WT=dataA_WT.append(df["AP4WT"], ignore_index=True)

dataA_Cav=df["AP1Cav"].append(df["AP2Cav"], ignore_index=True)
dataA_Cav=dataA_Cav.append(df["AP3Cav"], ignore_index=True)
dataA_Cav=dataA_Cav.append(df["AP4Cav"], ignore_index=True)

dataA_wafer=df["AW1WT"].append(df["AW2WT"], ignore_index=True)
dataA_wafer=dataA_wafer.append(df["AW3WT"], ignore_index=True)
dataA_wafer=dataA_wafer.append(df["AW4WT"], ignore_index=True)
dataA_wafer=dataA_wafer.append(df["AW1Cav"], ignore_index=True)
dataA_wafer=dataA_wafer.append(df["AW2Cav"], ignore_index=True)
dataA_wafer=dataA_wafer.append(df["AW3Cav"], ignore_index=True)
dataA_wafer=dataA_wafer.append(df["AW4Cav"], ignore_index=True)

# %%
#create new dataframe from the appended values and rename the columns 
dataA_WT=dataA_WT.to_frame()
dataA_WT.columns = ['WT']
dataA_Cav=dataA_Cav.to_frame()
dataA_Cav.columns = ['Cav']
dataA_wafer=dataA_wafer.to_frame()
dataA_wafer.columns = ['wafer']
frames_A = [dataA_wafer, dataA_WT, dataA_Cav]
result_A = pd.concat(frames_A,axis=1, sort=False)

# %%
#create violinplot from the new dataframe 'result_A'  to plot the Areas
sns.set(font_scale=2)
sns.set_style("whitegrid")
sns.set_palette("tab10")
ax1 = sns.violinplot(data=result_A)
ax1.set_ylabel('Area')

#save plot image; insert savename
plt.savefig("violinplot_AB_labeling_2_corrected_area.pdf", bbox_inches="tight")

