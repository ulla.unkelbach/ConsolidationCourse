# -*- coding: utf-8 -*-
"""
Created on Sat Sep 26 11:51:09 2020

@author: Ulla
"""

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
plt.style.use('seaborn-talk')
sns.set_palette("tab10")

# %%
#Load Data; insert the correct pathIn
pathIn=r"C:\Users\Ulla\Desktop\Fachvertiefung\Daten\200724_CLSM\daten_auswertung_intensity_corrected_py.csv"
df=pd.read_csv(pathIn)
#columns: AP= Area Patch, MP= MeanGreyValue Patch, AW= Area Wafer, MW= MeanGreyValue Wafer
#the number is the number of the evaluated patch
#WT or Cav determines between a wildtype and transfected/induced patch
#MeanGreyValues and Areas are determined with Particle Analysis in ImageJ
#all areas >3um^2 are removed from the original data

# %%
#append all MeanGrey values for each condition in one column
dataI_WT=df["MP1WT"].append(df["MP2WT"], ignore_index=True)
dataI_WT=dataI_WT.append(df["MP3WT"], ignore_index=True)
dataI_WT=dataI_WT.append(df["MP4WT"], ignore_index=True)

dataI_Cav=df["MP1Cav"].append(df["MP2Cav"], ignore_index=True)
dataI_Cav=dataI_Cav.append(df["MP3Cav"], ignore_index=True)
dataI_Cav=dataI_Cav.append(df["MP4Cav"], ignore_index=True)

dataI_wafer=df["MW1WT"].append(df["MW2WT"], ignore_index=True)
dataI_wafer=dataI_wafer.append(df["MW3WT"], ignore_index=True)
dataI_wafer=dataI_wafer.append(df["MW4WT"], ignore_index=True)
dataI_wafer=dataI_wafer.append(df["MW1Cav"], ignore_index=True)
dataI_wafer=dataI_wafer.append(df["MW2Cav"], ignore_index=True)
dataI_wafer=dataI_wafer.append(df["MW3Cav"], ignore_index=True)
dataI_wafer=dataI_wafer.append(df["MW4Cav"], ignore_index=True)

# %%
#create new dataframe from the appended values and rename the columns 
dataI_WT=dataI_WT.to_frame()
dataI_WT.columns = ['WT']
dataI_Cav=dataI_Cav.to_frame()
dataI_Cav.columns = ['Cav']
dataI_wafer=dataI_wafer.to_frame()
dataI_wafer.columns = ['wafer']
frames_I = [dataI_wafer, dataI_WT, dataI_Cav]
result_I = pd.concat(frames_I,axis=1, sort=False)


# %%
#create violinplot from the new dataframe 'result_I'  to plot the MeanGrayValues
sns.set(font_scale=2)
sns.set_style("whitegrid")
sns.set_palette("tab10")
ax = sns.violinplot(data=result_I)
ax.set_ylabel('Intensity')

#save plot image; insert savename
plt.savefig("violinplot_AB_labeling_2_corrected_Intens.pdf", bbox_inches="tight")


