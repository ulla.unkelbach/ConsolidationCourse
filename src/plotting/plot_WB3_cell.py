# -*- coding: utf-8 -*-
"""
Created on Fri Jul 17 10:36:24 2020

@author: Ulla
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.style.use('seaborn-talk')

# %%
#Load Data; insert the correct pathIn
pathIn=r"C:\Users\Ulla\Desktop\Fachvertiefung\Daten\200710_WB#3\auswertung_zellen\PlotValues_cells.csv"
df=pd.read_csv(pathIn)
#columns: Y0-> WT intensity, Y1-> Cav intensity, Y2-> Cav+ind intensity, X0-> background intensity
#intensities determined with ImageJ linescan, width 30 pts

# %%
#create Lineplot with substracted background
tmp1=df["Y0"]-df["Y3"]
tmp2=df["Y1"]-df["Y3"]
tmp3=df["Y2"]-df["Y3"]
plt.plot(df["X0"],tmp1, label="WT")
plt.plot(df["X0"],tmp2,  label="Ca$_V$")
plt.plot(df["X0"],tmp3, label="Ca$_V$ induced")
plt.ylabel('intenities')
plt.xlabel('distance')
plt.xlim(0,0.45)
plt.ylim(0,35500)
plt.grid()
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.title('Western Blot corrected Intensities - Cells')
plt.legend()

#save plot image; insert savename
plt.savefig('WB_intensities_cell_corr.png', dpi=300, bbox_inches='tight')
plt.show()

# %%
#create Barplot of the integrals from corrected intensities
integral1=sum(tmp1)
integral2=sum(tmp2)
integral3=sum(tmp3)
corr_intens_cell=[integral1,integral2, integral3]

bars = ('WT', 'Ca$_V$', 'Ca$_V$ ind')
y_pos = np.arange(len(bars))
plt.bar(y_pos, corr_intens_cell)
plt.xticks(y_pos, bars)
plt.ylabel('corrected intenities')
plt.ylim(0,3000000)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.title('Cell')

#save plot image; insert savename
plt.savefig('barplot_cell.png', dpi=300, bbox_inches='tight')
plt.show()