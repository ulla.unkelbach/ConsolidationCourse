# %%
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
sns.set_palette("tab10")

# %%
#Load Data; insert the correct pathIn
dataIN = r"C:\Users\Ulla\Desktop\Fachvertiefung\Daten\200730_CLSM\daten_area_py_power.csv"
df = pd.read_csv(dataIN, sep=";")
caseName = dataIN.split("/")[-1].split(".")[0]
#columns: the first column includes all Ratios, the second one the case
#the Ratios are determined by dividing the area of the outer ring by the area of the inner ring of the patch
#ROIs are determind in ImageJ

# %%
#removes all values =1
df2 = df[df.Ratio < 1]
# %%
#create boxplot  
ax1 = sns.boxplot(x="Case", y="Ratio", data=df2)
ax1.set(ylim=(-0.1, 1.1))
ax1.set_xlabel("Case",fontsize=15)
ax1.set_ylabel("Ratio",fontsize=15)
ax1.tick_params(labelsize=15)
plt.savefig(r"C:\Users\Ulla\Desktop\Git\ConsolidationCourse\reports\figures\boxplot_setup1.pdf", bbox_inches="tight")

# %%
